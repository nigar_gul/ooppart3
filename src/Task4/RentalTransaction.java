package Task4;
public class RentalTransaction {
    private Car car;
    private int rentalDays;
    private String customerName;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getRentalDays() {
        return rentalDays;
    }

    public void setRentalDays(int rentalDays) {
        this.rentalDays = rentalDays;
    }
    public RentalTransaction(Car car, String customerName, int rentalDays) {
        this.car = car;
        this.customerName = customerName;
        this.rentalDays = rentalDays;
    }
    public void displayTransactionInfo() {
        System.out.println("Name of the customer: " + customerName);
        System.out.println("Number of the rental days is: " + rentalDays);
        car.displayCarInfo();
        System.out.println("Rental cost of the car is: " + calculateRentalCost()+ "AZN");
    }
    public double calculateRentalCost() {
        return car.calculateRentalCharge(rentalDays);
    }
}
