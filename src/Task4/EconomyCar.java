package Task4;

public class EconomyCar extends Car {
    public double fuelEfficiency;

    public EconomyCar(String make, String model, int year, double rentalRate, double fuelEfficiency) {
        super(make, model, year, rentalRate);
        this.fuelEfficiency=fuelEfficiency;
    }


    public double getFuelEfficiency() {
        return fuelEfficiency;
    }

    public void setFuelEfficiency(double fuelEfficiency) {
        this.fuelEfficiency = fuelEfficiency;
    }

    @Override
    public double calculateRentalCharge(int numDays) {
        return getRentalRate()*numDays;
    }

    @Override
    public void rent(int numDays) {
    }

    @Override
    public void returnCar() {

    }
}
