package Task4;
public class MainCar {
    public static void main(String[] args) {

        EconomyCar economyCar = new EconomyCar("Toyota", "Corolla", 2015, 80.0, 45.0);
        economyCar.displayCarInfo();

        RentalTransaction rentalTransaction1 = new RentalTransaction(economyCar, "Nigar", 3);
        rentalTransaction1.displayTransactionInfo();
        rentalTransaction1.getCar().rent(3);
        rentalTransaction1.getCar().returnCar();

        String[] premiumFeatures = new String[0];
        LuxuryCar luxuryCar = new LuxuryCar("Porsche", "Cayenne", 2023, 300.0, premiumFeatures);
        premiumFeatures = new String[]{"Four-zone automatic climate control", "14-way electric power seats", "21-inch RS Spyder Design wheels"};
        luxuryCar.displayCarInfo();
        RentalTransaction rentalTransaction2 = new RentalTransaction(luxuryCar, "Banu", 5);
        rentalTransaction2.displayTransactionInfo();
        rentalTransaction2.getCar().rent(5);
        rentalTransaction2.getCar().returnCar();
    }
}
