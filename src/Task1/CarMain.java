package Task1;

public class CarMain {
    public static void main(String[] args) {
        Car car=new Car();
        car.setMake("Toyota");
        car.setModel("Prado");
        car.setYear(2023);
        car.setRentalPrice(5000);
        System.out.println("This car's make is " +car.getMake()+ ".");
        System.out.println("Its model is " +car.getModel()+ ".");
        System.out.println("It has designed in " +car.getYear()+ ".");
        System.out.println("And its rental price is " +car.getRentalPrice()+ ".");
    }

}
