package Task3;

public class VideoPlayer implements Playable{
    @Override
    public void play() {
        System.out.println("Video player played");
    }

    @Override
    public void stop() {
        System.out.println("Video player stopped");
    }
}
