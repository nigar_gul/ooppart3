package Task2;

import org.w3c.dom.ls.LSOutput;

public class EmployeeRunner {
    public static void main(String[] args) {
        Developer developer = new Developer("Gulnar", 2000, "English");
        Manager manager = new Manager("Nigar", 10000, "QA");
        developer.getDetails();
        manager.getDetails();
    }
}
