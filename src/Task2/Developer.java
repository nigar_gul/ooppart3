package Task2;


public class Developer extends Employee {
    public String language;
    public Developer(String name, double salary, String language) {
        super(name, salary);
        this.language=language;
    }
    public void setLanguage (String language) {
        this.language=language;
    }
    public String getLanguage ()
    {return language;}
    @Override
    public void getDetails() {
        System.out.println("Name: " +getName());
        System.out.println("Salary: " +getSalary());
        System.out.println("Language: " +getLanguage());
    }

}