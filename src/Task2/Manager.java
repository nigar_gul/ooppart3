package Task2;

public class Manager extends Employee {
    public Manager(String name, double salary, String department) {
        super(name, salary);
        this.department=department;
    }
    public String department;

    public Manager(String nigar, int salary) {
        super(nigar, salary);
    }

    public void setDepartment (String department) {
        this.department=department;
    }
    public String getDepartment ()
    {return department;}
    @Override
    public void getDetails() {
        System.out.println("Name: " +getName());
        System.out.println("Salary: " +getSalary());
        System.out.println("Department: " +getDepartment());
    }

}